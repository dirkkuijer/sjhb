<?php
 


//variabelen van Taart
$personenTaart = $soortTaart = $tekstSoortTaart = $aantalLagen = $decoratieTaart = $tekstDecoratieTaart = $prijsTaart =
$verwachtingTaart = $jaVerwachtingTaart = $neeVerwachtingTaart = $cijferTaart = "";

//variabelen van Cupcakes
$personenCupcakes = $decoratieCupcakes = $tekstDecoratieCupcakes = $wensCupcakes = $tekstWensCupcakes =
$prijsCupcakes = $smaakCupcakes = $cijferCupcakes = "";

//variabelen van Gebak & desserts
$personenGebak = $gebakBesteld = $wensGebak = $tekstWensGebak = $prijsGebak = $smaakGebak = $cijferGebak = "";

// variabelen van Workshop
$kinderen = $decoratieWorkshop = $goedWorkshop = $nietGoedWorkshop = $kwaliteitWorkshop = $verbeterWorkshop = $verbeterWorkshopJa
= $cijferWorkshop = "";

// variabelen van Communicatie
$contact = $tekstSoortContact = $wensCommunicatie = $tekstWensCommunicatie = $bestellenCommunicatie = $tekstBestellenCommunicatie
= $tekstSuggestiesCommunicatie = "";

if($_SERVER['REQUEST_METHOD'] == "POST") {


    if($_POST['submit']) {

             
        /*
         * formulierverwerking voor de optie Taart
         *
         * */
            if($_POST['product'] == "Taart") {
                $personenTaart = Gegevens($_POST['personenTaart']);
                $soortTaart = Gegevens($_POST['soortTaart']);
                
                // tekstvak niet ingevuld
                        if(!isset($_POST['tekstSoortTaart'])) {
                            $tekstSoortTaart = LegeWaarden();
                        } else {
                            
                            $tekstSoortTaart = Gegevens($_POST['tekstSoortTaart']); 
        
                        }
                        
                $aantalLagen = Gegevens($_POST['aantalLagen']);
               $decoratieTaart = Gegevens($_POST['decoratieTaart']);
               
                // tekvak niet ingevuld
                        if(!isset($_POST['tekstDecoratieTaart'])) {
                            $tekstDecoratieTaart = LegeWaarden();
                        } else {
                            
                        $tekstDecoratieTaart = Gegevens($_POST['tekstDecoratieTaart']);
        
                        }
                
               $prijsTaart = Gegevens($_POST['prijsTaart']);
                $verwachtingTaart = Gegevens($_POST['verwachtingTaart']);
                    
                   // deze heeft 2 tekstvakken.
                        if(!isset($_POST['jaVerwachtingTaart'])) {
                          
                            $jaVerwachtingTaart = LegeWaarden();
                            
                            } else {
                                $jaVerwachtingTaart = Gegevens($_POST['jaVerwachtingTaart']);                        
                                 }
                       
                        
                        if(!isset( $_POST['neeVerwachtingTaart'])) {
                             $neeVerwachtingTaart = LegeWaarden();
                       
                            } else {                        
                                $neeVerwachtingTaart = Gegevens($_POST['neeVerwachtingTaart']);
                                }
                  
                    $cijferTaart = Gegevens($_POST['cijferTaart']);
   
   $weergeven =  weergeven($personenTaart, $soortTaart, $tekstSoortTaart, $aantalLagen, $decoratieTaart, 
     $tekstDecoratieTaart, $prijsTaart, $verwachtingTaart, $jaVerwachtingTaart, $neeVerwachtingTaart, $cijferTaart);
            } 
            
            /*
             * formulierverwerking voor de optie Cupcakes
             * 
             * */
                elseif($_POST['product'] == "Cupcakes") {
                    $personenCupcakes = Gegevens($_POST['personenCupcakes']);
                    $decoratieCupcakes = Gegevens($_POST['decoratieCupcakes']);
                    
                    // tekstvak niet ingevuld
                    if(!isset($_POST['tekstDecoratieCupcakes'])) {
                        $tekstDecoratieCupcakes = LegeWaarden();
                    } else {
                        
                        $tekstDecoratieCupcakes = Gegevens($_POST['tekstDecoratieCupcakes']);
                        
                    }
                   
                    $wensCupcakes = Gegevens($_POST['wensCupcakes']);

                    // tekstvak niet ingevuld
                    if(!isset($_POST['tekstWensCupcakes'])) {
                        $tekstWensCupcakes = LegeWaarden();
                    } else {
                        
                        $tekstWensCupcakes = Gegevens($_POST['tekstWensCupcakes']);
                        
                    }
                    $prijsCupcakes = Gegevens($_POST['prijsCupcakes']);
                    $smaakCupcakes = Gegevens($_POST['smaakCupcakes']);
                    $cijferCupcakes =  Gegevens($_POST['cijferCupcakes']);
                    
            $weergeven = "
                        Cupcakes:
                        1. Voor hoeveel personen had u besteld? $personenCupcakes
                        2. Hadden deze cupcakes specifieke decoratie? $decoratieCupcakes
                        Kunt u deze decoratie omschrijven? $tekstDecoratieCupcakes
                        3. Waren de cupcakes naar wens? $wensCupcakes
                        Wat was er niet naar wens? $tekstWensCupcakes
                        4. Hoeveel hebben de cupcakes gekost? $prijsCupcakes
                        5. Voldeden de cupcakes aan uw verwachtingen? $smaakCupcakes
                        6. Welk cijfer zou u willen geven voor onze prijs/kwaliteit verhouding? $cijferCupcakes
                        ";
                    
                } 
            

         
                 // formulierverwerking voor de optie Gebak en Desserts

                    elseif($_POST['product'] == "Gebak en Desserts") {   
                  
                      $personenGebak = Gegevens($_POST['personenGebak']);
                                      
                      // tekstvak niet ingevuld
                      if(!isset($_POST['gebakBesteld'])) {
                         $gebakBesteld = LegeWaarden();
                      } else {
                          
                          $gebakBesteld = Gegevens($_POST['gebakBesteld']);
                          
                      }
                      
                      $wensGebak = Gegevens($_POST['wensGebak']);
                      
                      // tekstvak niet ingevuld
                      if(!isset($_POST['$ekstWensGebak'])) {
                         $tekstWensGebak = LegeWaarden();
                      } else {
                          
                          $tekstWensGebak = Gegevens($_POST['tekstWensGebak']);
                          
                      }
                     $prijsGebak = Gegevens($_POST['prijsGebak']);
                      $smaakGebak = Gegevens($_POST['smaakGebak']);
               
       // tekstvak niet ingevuld
                      if(!isset($_POST['smaakGebak'])) {
                         $smaakGebak = LegeWaarden();
                      } else {
                          
                          $smaakGebak = Gegevens($_POST['smaakGebak']);
                          
                      }
                      $smaakGebak = Gegevens($_POST['smaakGebak']);
                      $cijferGebak = Gegevens($_POST['cijferGebak']);
        
         $weergeven = "
                        Gebak:
                        1. Voor hoeveel personen had u besteld? $personenGebak
                        2. Wat voor soort gebak of dessert had u besteld?  $gebakBesteld
                        3. Was het gebak of dessert naar wens? $wensGebak
                           Wat was er niet naar wens? $tekstWensGebak
                        4. Hoeveel kostte het gebak of dessert? $prijsGebak 
                        5. Voldeed het gebak of dessert aan uw verwachtingen? (bv. decoratie of smaak etc.)? $smaakGebak
                        6. Welk cijfer zou u willen geven voor onze prijs/kwaliteit verhouding? $cijferGebak
                        ";      
                    } 
               
                    /*
                     * formulierverwerking voor de optie Workshop
                     *
                     * */
                        elseif($_POST['product'] == "Workshop") {  
                        $kinderen = Gegevens($_POST['kinderen']);
                        $decoratieWorkshop =  Gegevens($_POST['decoratieWorkshop']);
                        $goedWorkshop =  Gegevens($_POST['goedWorkshop']);
                        $nietGoedWorkshop =  Gegevens($_POST['nietGoedWorkshop']);
                        $kwaliteitWorkshop =  Gegevens($_POST['kwaliteitWorkshop']);
                        $verbeterWorkshop =  Gegevens($_POST['verbeterWorkshop']);
                        $verbeterWorkshopJa = Gegevens($_POST['verbeterWorkshopJa']);
                        $cijferWorkshop = Gegevens($_POST['cijferWorkshop']);

                $weergeven = "
                            Workshop:
                            1. Wat vonden de kinderen ervan? $kinderen
                            2. Was er genoeg decoratie, en hoe was de diversiteit daarvan? $decoratieWorkshop
                            3. Wat vond u goed aan de workshop? $goedWorkshop
                            4. Wat vond u minder goed aan de workshop? $nietGoedWorkshop
                            5. Wat vind u van de prijs/kwaliteit verhouding van de workshop? $kwaliteitWorkshop
                            6. Heeft u suggesties ter verbetering van de workshop? $verbeterWorkshopJa
                            7. Welk cijfer zou u willen geven voor onze prijs/kwaliteit verhouding?  $cijferWorkshop
                            ";
                        } 
                        
                    // onverwachte fouten.
                        else {
                                 echo "De selectie van het soort product of dienst is niet goed gegaan, ga terug en probeer het alstublieft opnieuw!";
                              }
    } else {
            echo 'er is een fout opgetreden, probeer het alstublieft opnieuw!';
        }
        
        /********************
         * verwerking van de variabelen van communicatie
         * 
         *De site doet het verder. Waar je naar moet kijken is hoe je de variabelen van communicatie erbij inschiet in het bericht
         *van de mail. Schrijf nog een testscript of zoek op hoe je dat moet doen hiervoor.
         *Goed leermoment denk ik.
         * */
        
          $contact = Gegevens($_POST['contact']);
        if(!isset($_POST['tekstSoortContact'])) {
            $tekstSoortContact = LegeWaarden();
        } else {
            $tekstSoortContact = Gegevens($_POST['tekstSoortContact']);
            }
            
        $wensCommunicatie = Gegevens($_POST['wensCommunicatie']);
        
        
        if(!isset($_POST['tekstWensCommunicatie'])) {
            $tekstWensCommunicatie = LegeWaarden();
        } else {
            $tekstWensCommunicatie = Gegevens($_POST['tekstWensCommunicatie']);
        }
        
        $bestellenCommunicatie = Gegevens($_POST['bestellenCommunicatie']);
        
       
        if(!isset($_POST['tekstBestellenCommunicatie'])) {
            $tekstBestellenCommunicatie = LegeWaarden();
        } else {
           $tekstBestellenCommunicatie = Gegevens($_POST['tekstBestellenCommunicatie']);
        }
        
        if(!isset($_POST['suggestieCommunicatie'])) {
            $suggestieCommunicatie = LegeWaarden();
        } else {
          $suggestieCommunicatie = Gegevens($_POST['suggestieCommunicatie']);
        }
        
        if(!isset($_POST['tekstSuggestiesCommunicatie'])) {
           $tekstSuggestiesCommunicatie = LegeWaarden();
        } else {
            $tekstSuggestiesCommunicatie = Gegevens($_POST['tekstSuggestiesCommunicatie']);
        }
         
        $weergevenCommunicatie = " 
        Communicatie:
        1. Hoe heeft u contact gehad met ons? $contact
           Hoe heeft u dan contact met ons gehad? $tekstSoortContact 
        2. Verliep dit naar wens? $wensCommunicatie
           Wat verliep er niet naar wens? $tekstWensCommunicatie
        3. Zou u in de toekomst nogmaals iets willen bestellen bij ons? $bestellenCommunicatie
           Wat is de reden? $tekstBestellenCommunicatie
        4. Heeft u nog suggesties voor ons? $suggestieCommunicatie
           suggesties: $tekstSuggestiesCommunicatie
        ";
/*
 * Het mailtje in mekaar zetten.*/
        
        $to = "vragenlijst@sjhomebakery.nl";
        $subject = "Vragenlijst workshop";
        $message = "

$weergeven
$weergevenCommunicatie
";
    $headers = 'From:vragenlijst@sjhomebakery.nl';
    $header = "Content-type: text/html; charset=utf-8";
    
    $verstuurd = mail($to, $subject, $message, $headers);
    

    
    echo '<link rel=stylesheet href="styleDirk.css">
    <img src="logo3.jpeg" width="200" height="200" style="box-shadow: 3px 3px 25px black;">';

    if($verstuurd) {
     echo' <p>
                Uw bericht is goed verstuurd. Bedankt voor het invullen.
                U kunt nu de browser sluiten.
            </p>';
    }
    else {
        echo '  <p>
                    Er is iets misgegaan met het versturen. Ga terug en probeer het opnieuw alstublieft!
                </p>';

    }



}
// functie mocht het tekstvak niet gebruikt worden. Dan wordt daar deze waarde aan toegekend.
    function LegeWaarden() {
        $leeg = "Niet geselecteerd";
        
        return $leeg;
    }

    // gegevens filteren van foutieve input.
    function Gegevens($gegevens) {
        $gegevens = trim($gegevens);
        $gegevens = stripslashes($gegevens);
        $gegevens = (htmlspecialchars($gegevens));
        return $gegevens;
    }
    
    function weergeven($personenTaart, $soortTaart, $tekstSoortTaart, $aantalLagen, $decoratieTaart, $tekstDecoratieTaart,
        $prijsTaart, $verwachtingTaart, $jaVerwachtingTaart, $neeVerwachtingTaart,  $cijferTaart) {
      // hier echo stond weergeven testen!      
       $weergeven = "
        Taart:
        1. Voor hoeveel personen had u taart besteld? $personenTaart
        2. Wat voor taart was het? $soortTaart
           Geef een korte omschrijving. $tekstSoortTaart
        3. Hoeveel lagen had deze taart? $aantalLagen 
        4. Had deze taart speciale door u gekozen decoraties? $decoratieTaart
           Kunt u deze decoratie omschrijven? $tekstDecoratieTaart
        5. Hoeveel kostte deze taart? $prijsTaart 
        6. Voldeed deze taart aan uw verwachtingen? $verwachtingTaart 
           Wat beviel u aan de taart? $jaVerwachtingTaart 
           Wat was er niet goed? $neeVerwachtingTaart 
           7. Welk cijfer zou u willen geven voor onze prijs/kwaliteit verhouding? $cijferTaart 
        ";
            
            return $weergeven;
            
    }

   
?>