<!DOCTYPE html>

<html lang="nl">
	<head>
    	<meta charset="UTF-8">
    	
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    	<script>
    	var app = angular.module('vragenlijst', []);</script>
 
    	<link rel="stylesheet" href="styleDirk.css">
    	
    	<title>S J Home Bakery Vragenlijst</title>	
	
	</head>

<body ng-app="vragenlijst">

	<header>

		<img src="logo3.jpeg" width="200" height="200" alt="Logo" style="box-shadow: 3px 3px 25px black;">

		<h1>Vragenlijst over producten of diensten.</h1>
	</header>
	
<main>

<article >
	Bedankt dat u de vragenlijst wilt invullen. We zien graag dat u de vragen zo eerlijk mogelijk invult om zo onze
	dienstverlening en producten te verbeteren. U krijgt een aantal vragen over het product wat u onlangs heeft mogen ervaren
	en over de communicatie.<p>
	
	Het is de bedoeling dat u zoveel mogelijk afgaat op uw laatste ervaring met ons product of dienst.
	De antwoorden worden via een e-mail anoniem naar ons verstuurd.
	Uw privacy is dus gewaarborgd. </p>
	
</article>
	<form name="vragenlijst" method="POST" action="verwerking.php" ng-model="vragenlijst">

		<table>
			<tr>
				<td>
			Welke dienst of product heeft u afgenomen?
				</td>
		</tr><tr>
				<td>
        			<input type="radio" name="product"  ng-model="soort" value="Taart" required>Taart<br>
        			<input type="radio" name="product"  ng-model="soort" value="Cupcakes" required>Cupcakes<br>
        			<input type="radio" name="product"  ng-model="soort" value="Gebak en Desserts" required>Gebak & desserts<br>
        			<input type="radio" name="product"  ng-model="soort" value="Workshop" required>Workshop<br>
				</td>
			</tr>
			
			<tr><td>
		
				<div ng-switch="soort">
					<div ng-switch-when="Taart">
						<h3>U krijgt zeven vragen. En nog over vier communicatie.</h3>
						<div ng-include="'taart.html'"></div>
						<div ng-include="'communicatie.html'"></div>
					</div>
					<div ng-switch-when="Cupcakes">
						<h3>U krijgt zes vragen. En nog over vier communicatie.</h3>
						<div ng-include="'cupcakes.html'"></div>
						<div ng-include="'communicatie.html'"></div>
					</div>
					<div ng-switch-when="Gebak en Desserts">
						<h3>U krijgt zeven vragen. En nog over vier communicatie.</h3>
						<div ng-include="'gebak.html'"></div>
						<div ng-include="'communicatie.html'"></div>
					</div>
					<div ng-switch-when="Workshop">
						<h3>U krijgt zeven vragen. En nog over vier communicatie.</h3>
						<div ng-include="'workshop.html'"></div>
						<div ng-include="'communicatie.html'"></div>
					</div>
				</div>
			</td>
		</tr><tr>
			<td>
                	<!-- extra rij om zo ruimte tussen de knoppen en de velden te krijgen. -->				
			</td>
		</tr><tr>	
			<td>
				<div ng-model="soort">
					<div ng-show="soort" style="text-align: center;">
						<input type="submit" name="submit" ng-model="submit" value="Verzenden">			
					</div>
				</div>	
		</td>	
	</tr>
					
					
		
</table>
	</form>
</main>
</body>
</html>